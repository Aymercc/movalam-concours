State Group	ID	Name			Wwise Object Path	Notes
	1021618141	MusicState			\Default Work Unit\MusicState	

State	ID	Name	State Group			Notes
	89505537	Gameplay	MusicState			
	748895195	None	MusicState			
	2706359573	Borne	MusicState			
	3604647259	MainMenu	MusicState			
	4158285989	GameOver	MusicState			

Game Parameter	ID	Name			Wwise Object Path	Notes
	167863329	EffectVolumeParameter			\Default Work Unit\EffectVolumeParameter	
	1738347081	MusicVolumeParameter			\Default Work Unit\MusicVolumeParameter	

Audio Bus	ID	Name			Wwise Object Path	Notes
	493417818	Music Audio Bus			\Default Work Unit\Music Audio Bus	
	3803692087	Master Audio Bus			\Default Work Unit\Master Audio Bus	

Audio Devices	ID	Name	Type				Notes
	2317455096	No_Output	No Output			
	3859886410	System	System			

