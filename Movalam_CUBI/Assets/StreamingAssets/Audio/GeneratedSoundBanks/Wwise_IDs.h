/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID CLICKASSISTANT = 4257594657U;
        static const AkUniqueID CLICKBUTTONDOOR = 225502019U;
        static const AkUniqueID DETECTED = 298938147U;
        static const AkUniqueID DETECTION = 882108042U;
        static const AkUniqueID DETECTIONPORTE = 1026403484U;
        static const AkUniqueID FAIL = 2596272617U;
        static const AkUniqueID FOOTSTEP = 1866025847U;
        static const AkUniqueID LASER = 3982605422U;
        static const AkUniqueID LDDESACTIVATION = 1560767711U;
        static const AkUniqueID NOTIFICATION = 3900109056U;
        static const AkUniqueID OPENWINDOW = 3712166233U;
        static const AkUniqueID PLAYMUSIC = 417627684U;
        static const AkUniqueID SUCCESS = 3625060726U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace MUSICSTATE
        {
            static const AkUniqueID GROUP = 1021618141U;

            namespace STATE
            {
                static const AkUniqueID BORNE = 2706359573U;
                static const AkUniqueID GAMEOVER = 4158285989U;
                static const AkUniqueID GAMEPLAY = 89505537U;
                static const AkUniqueID MAINMENU = 3604647259U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace MUSICSTATE

    } // namespace STATES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID EFFECTVOLUMEPARAMETER = 167863329U;
        static const AkUniqueID MUSICVOLUMEPARAMETER = 1738347081U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC_AUDIO_BUS = 493417818U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
