Cadres joueurs

90x90 pour la pp steam
120x120 pour le cadre complet
120x145 pour le cadre lors de la sélection
70px du bas et de la gauche, 20px d'écart entre chaque

Emotes

400x900 pour le grand cadre, 70px du bas et de la droite
160x90 pour les cadres d'emotes, 30px des côtés du cadre, 20px du haut et bas du cadre et entre les emotes
400x20 pour la barre de bande passante
70px du haut et de la droite