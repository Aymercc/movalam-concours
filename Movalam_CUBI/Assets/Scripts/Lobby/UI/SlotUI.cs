using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Steamworks;

public class SlotUI : MonoBehaviour
{
    //public delegate void SwitchSlot(SteamId steamId);
    //public static SwitchSlot OnSwitchSlot;
    public Image slotAvatar;
    public TextMeshProUGUI slotName;
    public Button slotReadyButton;
    public Image slotReadyCheck;
    public Button slotButton;
    public int slotIndex;
    public SteamId steamId;
    public Image questionMark;
    public Image hostCrown;

    public bool isAvailable {
        get;
        private set;
    }

    private bool ready;
    public bool isReady {
        get { return ready; }
        set {
            ready = value;
            slotReadyCheck.enabled = value;
        }
    }

    private void Awake()
    {
        UnloadPlayer();
    }

    protected void SetPlayerName(string name)
    {
        slotName.text = name;
    }

    protected void SetPlayerAvatar(Sprite avatar)
    {
        slotAvatar.sprite = avatar;
    }

    public virtual void UnloadPlayer()
    {
        steamId = 0;
        SetPlayerName("Player Slot Available");
        slotAvatar.sprite = null;
        slotAvatar.gameObject.SetActive(false);
        slotReadyButton.gameObject.SetActive(false);
        hostCrown.gameObject.SetActive(false);
        slotButton.interactable = true;
        questionMark.enabled = true;
        isAvailable = true;
    }

    public virtual void LoadPlayer(Slot slot)
    {
        questionMark.enabled = false;
        steamId = slot.takenBy;
        slotAvatar.gameObject.SetActive(true);
        if(LobbyManager.Instance.currentLobby.Owner.Id != slot.takenBy)
        {
            slotReadyButton.gameObject.SetActive(true);
            hostCrown.gameObject.SetActive(false);
        }
        else
        {
            slotReadyButton.gameObject.SetActive(false);
            hostCrown.gameObject.SetActive(true);
        }
        UpdateProfile(LobbyManager.Instance.profiles[slot.takenBy]);
        slotButton.interactable = false;
        isReady = slot.isReady;
        isAvailable = false;
        LobbyManager.Instance.profiles[slot.takenBy].OnProfileUpdated += UpdateProfile;
    }

    public virtual void UpdateProfile(SteamProfile profile)
    {
        SetPlayerName(profile.steamName);
        SetPlayerAvatar(profile.steamAvatar);
    }

    public void SwitchSlotTrigger()
    {
        if (isAvailable)
        {
            LobbyManager.Instance.currentLobby.SetMemberData("slotIndex", slotIndex.ToString());
            LobbyManager.Instance.currentLobby.SetMemberData("readyState", (false).ToString());
        }
    }

    //public void ToggleReadyStateTrigger()
    //{
    //    if(steamId == SteamManager.Instance.localPlayer.steamId)
    //    {
    //    }
    //}
}
