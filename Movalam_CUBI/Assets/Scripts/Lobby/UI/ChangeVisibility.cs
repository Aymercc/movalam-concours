using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ChangeVisibility : MonoBehaviour
{
    public bool invisible = false;
    private TextMeshProUGUI guiText;

    private void Start()
    {
        guiText = GetComponent<TextMeshProUGUI>();
    }

    public void Change()
    {
        invisible = !invisible;
        ChangeText();
    }

    public void ChangeText()
    {
        try
        {
            if (guiText.text == "VISIBLE")
                guiText.text = "INVISIBLE";
            else if (guiText.text == "INVISIBLE")
                guiText.text = "VISIBLE";
        }
        catch { }
    }
}
