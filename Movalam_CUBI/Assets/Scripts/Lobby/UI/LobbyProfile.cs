using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Steamworks.Data;
using TMPro;

public class LobbyProfile : MonoBehaviour
{
    public Transform SelectedLobbyProfile;
    public TextMeshProUGUI lobbyName;
    public TextMeshProUGUI lobbySlots;
    public LevelAssetUI levelAssetUI;
    public Button joinButton;

    void Awake()
    {
        ListLobbies.OnLobbySelected += LoadLobby;
        Unload();
    }

    private void OnDestroy()
    {
        ListLobbies.OnLobbySelected -= LoadLobby;
    }

    public void LoadLobby(Lobby lobby)
    {
        string lobbyLevel = lobby.GetData("level");
        LevelAsset levelAsset = LevelAssetsLoader.GetLevelAsset(lobbyLevel);

        if (SelectedLobbyProfile)
        {
            SelectedLobbyProfile.gameObject.SetActive(true);
        }

        if(lobbyName)
            lobbyName.text = lobby.GetData("name");
        if(levelAssetUI)
            levelAssetUI.LoadLevelAsset(levelAsset);
        if (lobbySlots)
        {
            lobbySlots.text = lobby.MemberCount + "/" + lobby.MaxMembers;
            lobbySlots.color = lobby.MemberCount < lobby.MaxMembers ? UnityEngine.Color.green : UnityEngine.Color.red;
        }
        if (joinButton)
        {
            joinButton.onClick.RemoveAllListeners();
            joinButton.onClick.AddListener(() =>
            {
                lobby.Join().ContinueWith(roomEnterResult =>
                {
                    if (roomEnterResult.Result != Steamworks.RoomEnter.Success)
                    {
                        Debug.Log("join lobby failed");
                        LobbyManager.Instance.OnJoinLobbyFailed(roomEnterResult.Result);
                    }   
                });
            });
        }
    }

    public void Unload()
    {
        if (lobbyName)
            lobbyName.text = "";
        if (levelAssetUI)
            levelAssetUI.UnloadLevelAsset();
        if (lobbySlots)
        {
            lobbySlots.text = "";
            lobbySlots.color = UnityEngine.Color.white;
        }
        if (joinButton)
            joinButton.onClick.RemoveAllListeners();
        if(SelectedLobbyProfile)
            SelectedLobbyProfile.gameObject.SetActive(false);
    }
}
