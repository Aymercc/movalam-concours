using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Steamworks.Data;
using Image = UnityEngine.UI.Image;

public class LobbyRow : MonoBehaviour
{
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI levelText;
    public TextMeshProUGUI slotsText;
    public TextMeshProUGUI pingText;
    public Image levelImage;
    public Button rowButton;
    private Lobby lobby;

    public void LoadLobby(Lobby lobby)
    {
        LevelAsset lobbyLevel = LevelAssetsLoader.GetLevelAsset(lobby.GetData("level"));
        this.lobby = lobby;
        nameText.text = lobby.GetData("name") != "" ? lobby.GetData("name") : "no name";
        levelText.text = lobbyLevel.displayName;
        slotsText.text = lobby.MemberCount + "/" + lobby.MaxMembers;
        levelImage.sprite = lobbyLevel.image;

        //If slots available text in green else no available text in slot red
        //slotsText.color = lobby.MemberCount < lobby.MaxMembers ? UnityEngine.Color.green : UnityEngine.Color.red;
        //pingText.text = lobby.ping
    }
}
