using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GameRuleSet : ScriptableObject
{
    /// <summary>
    ///     Initial time for a player before detection
    /// </summary>
    /// <value>in seconds</value>
    [Header("Detection Rules")]
    public float detectionTime;

    /// <summary>
    ///     Time before a agent is dead after his detection
    /// </summary>
    /// <value>in seconds</value>
    public float timeBeforeAgentDead;

    /// <summary>
    ///     Factor used to reduce the detection time when a agent is dead (agent isn't saved by assistant)
    /// </summary>
    /// <value>Range 0 to 1</value>
    [Range(0,1)]
    public float detectionTimeReductionFactor;
}
