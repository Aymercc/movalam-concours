using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class LevelAsset : ScriptableObject
{
    public string displayName;
    [TextArea]
    public string description;
    public Sprite image;
    public SceneField sceneField;
    public int AssitantCount;
    public int AgentCount;
    public int TotalCount
    {
        get { return AssitantCount + AgentCount; }
        
    }
    public Difficulty.Difficulties difficulty;
    public GameRuleSet levelRuleSet;

    /// <summary>
    ///     If true this level is only gonna be loaded in editor
    /// </summary>
    public bool IsDevLevel = false;
}
