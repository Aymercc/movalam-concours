using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class LevelAssetUI : MonoBehaviour
{
    public LevelAsset levelAsset;
    public TextMeshProUGUI displayName;
    public TextMeshProUGUI description;
    public TextMeshProUGUI rolesCount;
    public TextMeshProUGUI displayDifficulty;
    public Image levelImage;
    public Button cardSelectButton;
    public GetLevelInfo levelInfo;
    public GameObject colorBackground;

    public void ActiveBackground()
    {
        colorBackground.SetActive(true);
    }

    public void DeactiveBackground()
    {
        colorBackground.SetActive(false);
    }

    private void OnEnable()
    {
        DeactiveBackground();
    }

    public void LoadLevelAsset(LevelAsset levelAsset)
    {
        this.levelAsset = levelAsset;
        if (displayName)
            displayName.text = levelAsset.displayName;

        if(description)
            description.text = levelAsset.description;

        if(rolesCount)
            rolesCount.text = $"{levelAsset.AssitantCount} Assistant\n{levelAsset.AgentCount} Agents";

        if(levelImage)
            levelImage.sprite = levelAsset.image;

        if (displayDifficulty)
        {
            displayDifficulty.text = levelAsset.difficulty.ToString();
            displayDifficulty.color = Difficulty.DifficultiesColor[(int)levelAsset.difficulty];
        }

        if(levelInfo)
        {
            if(levelImage)
                levelInfo.levelImage.sprite = levelImage.sprite;
            if(description)
                levelInfo.description.text = "> " + description.text;
            if(displayName)
                levelInfo.levelName.text = displayName.text;
            if(displayDifficulty)
                levelInfo.difficulty.text = displayDifficulty.text;
        }
    }

    public void UnloadLevelAsset()
    {

        this.levelAsset = null;
        if (displayName)
            displayName.text = "";

        if (description)
            description.text = "";

        if (rolesCount)
            rolesCount.text = "";

        if (levelImage)
            levelImage.sprite = null;

        if (displayDifficulty)
        {
            displayDifficulty.text = "";
            displayDifficulty.color = Color.white;
        }
    }
}
