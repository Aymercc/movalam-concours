using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class MurTransparent : MonoBehaviour
{
    public static GameObject player;
    private MeshRenderer[] meshRenderes;
    private float stencilValue;
    public bool printAll = false;

    private void Start()
    {
        meshRenderes = GetComponentsInChildren<MeshRenderer>();
        if (!NetworkClient.active)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }

    }

    void Update()
    {
        if (!player) return;

        //Degug
        if (printAll)
        {
            Debug.Log("mur : "+transform.position.z);
            Debug.Log("player : "+player.transform.position.z);
            Debug.Log((transform.rotation.eulerAngles.y / 90) % 2);
        }

        //If the player is behind the wall
        if (player.transform.position.z>transform.position.z)
        {
            //if the player wasn't behind the wall in previous frame
            if (stencilValue != 2.0f)
            {
                stencilValue = 2.0f;
                //set material to desappear 
                foreach (MeshRenderer mesh in meshRenderes)
                {
                    mesh.material.SetFloat("_StencilRef", stencilValue);
                }
            }
        }
        //if the player is nots behind the wall
        else
        {
            //if the player was behind the wall in previous frame
            if (stencilValue != 3.0f)
            {
                stencilValue = 3.0f;
                //set material to not desappear
                foreach (MeshRenderer mesh in meshRenderes)
                {
                    mesh.material.SetFloat("_StencilRef", stencilValue);
                }
            }
        }
    }

    
}
