using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CodeTerminal : GenericTerminal
{
    [Header("Code specificities")]
    [SerializeField] private string code;                          // Le code affich� sur la borne
    [SerializeField] private Text codeText;                        // Le component texte du canvas
    [SerializeField] private GameObject porteCode;                 // Une r�f�rence sur la porte � code li�e � ce terminal (� utiliser pour l'outline)


    //pour le outline
    private Outline setCouleur;
    private Outline setCorespondance;
    private int CodeCouleur;
    private Color CouleurChoisi;

   
    public override void Decrypt()
    {
        base.Decrypt();
        interactionWindowAnimator.SetBool("first", true);
        interactionWindowAnimator.SetBool("starting", true);
    }

    void Start()
    {
        setCouleur = gameObject.GetComponentInChildren<Outline>();
        CodeCouleur = DeterminerCouleur.Instance.NombreCouleur();
        CouleurChoisi = DeterminerCouleur.Instance.ChoisirCouleur(CodeCouleur);
        setCouleur.OutlineCouleur(CouleurChoisi);

        setCorespondance = porteCode.gameObject.GetComponentInChildren<Outline>();
        setCorespondance.OutlineCouleur(CouleurChoisi);

        string builder = "";
        for (int i = 0; i < code.Length; i++)
        {
            builder += code[i] + " ";
        }
        codeText.text = builder;

        if (porteCode)
            porteCode.GetComponent<DecodeurPorte>().curPassword = code;
    }

    public override void EndInteraction()
    {
        base.EndInteraction();
        if (!crypted)
            interactionWindowAnimator.SetBool("first", false);
    }
}
