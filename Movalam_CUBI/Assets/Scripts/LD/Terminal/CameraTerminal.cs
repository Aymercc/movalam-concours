using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTerminal : GenericTerminal
{
    [Header("Camera specificities")]
    public GameObject cameraToDeactivate;                     // La cam�ra � desactiver

    public delegate void DesactivateCameraEvent();
    public DesactivateCameraEvent OnDesactivateCameraEvent;
    public AK.Wwise.Event wwiseEventDisableLD;

    //pour le outline
    private Outline setCouleur;
    private Outline[] setCorespondance;
    private int CodeCouleur;
    private Color CouleurChoisi;



    private void Start()
    {
        setCouleur = gameObject.GetComponentInChildren<Outline>();
        CodeCouleur = DeterminerCouleur.Instance.NombreCouleur();
        CouleurChoisi = DeterminerCouleur.Instance.ChoisirCouleur(CodeCouleur);
        setCouleur.OutlineCouleur(CouleurChoisi);

        setCorespondance = cameraToDeactivate.gameObject.GetComponentsInChildren<Outline>();
        foreach (var outline in setCorespondance)
        {
            outline.OutlineCouleur(CouleurChoisi);
        }
    }

    public override void Interaction()
    {
        if (!crypted)
        {
            DeactivateCamera();
        }

        base.Interaction();
    }

    public override void Decrypt()
    {
        base.Decrypt();
        DeactivateCamera();
        interactionWindowAnimator.SetBool("first", true);
        interactionWindowAnimator.SetBool("starting", true);
    }

    public void DeactivateCamera()
    {
        OnDesactivateCameraEvent?.Invoke();
        wwiseEventDisableLD.Post(cameraToDeactivate);
        cameraToDeactivate.SetActive(false);
        //EndInteraction();
    }

    public override void EndInteraction()
    {
        base.EndInteraction();
        if (!crypted)
            interactionWindowAnimator.SetBool("first", false);
    }
}
