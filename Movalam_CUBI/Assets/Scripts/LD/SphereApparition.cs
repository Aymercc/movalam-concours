using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereApparition : MonoBehaviour
{
    #region Public Variables

    [Header("Scale Settings")]
    public Vector3 maxScale;                            //Scale of the sphere view if the player is behind a wall
    public AnimationCurve animationCurveFadeIn;         //AnimationCurve of the apparition of sphere
    public AnimationCurve animationCurveFadeOut;        //AnimationCurve of the disappearance of sphere
    [Header("Raycasts Settings")]
    public LayerMask obstacleMask;                       //Layer which collide with the raycast
    public float rayOfBeginRaycast;                     //The distance on X of the raycast between the player
    public Vector3 raycastDir;                          //Direction of the raycast

    #endregion

    #region Private Variables


    private RaycastHit firstHit;                        //Hit of the first raycast(left)
    private RaycastHit secondHit;                       //Hit of the second raycat(right)
    private float timeFadeIn = 0;                       //time to calculate the elapsed time during the fadeIn
    private float timeFadeOut = 0;                      //time to calculate the elapsed time during the fadeOut
    private bool appearFirst = false;                   //Check if first Raycast collide a good gameObject
    private bool appearSecond = false;                  //Check if second Raycast collide a good gameObject

    #endregion


    void Update()
    {
        //First Raycast (Left)
        if (Physics.Raycast(transform.position - new Vector3(rayOfBeginRaycast, 0, 0), raycastDir, out firstHit, 1500, obstacleMask))
        {
            //Debug
            Debug.DrawLine(transform.position - new Vector3(rayOfBeginRaycast, 0, 0), firstHit.point, Color.red);
            //sphere can appear
            appearFirst = true;
        }
        else
        {
            //in Left there are no wall
            appearFirst = false;
        }

        //Second Raycast (Right)
        if (Physics.Raycast(transform.position + new Vector3(rayOfBeginRaycast, 0, 0), raycastDir, out secondHit, 1500, obstacleMask))
        {
            //Debug
            Debug.DrawLine(transform.position + new Vector3(rayOfBeginRaycast, 0, 0), secondHit.point, Color.red);
            //Sphere can appear
            appearSecond = true;
        }
        else
        {
            //in right there are no wall
            appearSecond = false;
        }

        //if there are a wall (left or right or both), fade in
        if ((appearFirst || appearSecond) && timeFadeIn < animationCurveFadeIn.keys[animationCurveFadeIn.length - 1].time)
        {
            Fade(ref timeFadeIn, animationCurveFadeIn, ref timeFadeOut);
        }
        //if there aren't a wall, fade out
        else if (!(appearFirst || appearSecond) && timeFadeOut < animationCurveFadeOut.keys[animationCurveFadeOut.length - 1].time)
        {
            Fade(ref timeFadeOut, animationCurveFadeOut, ref timeFadeIn);
        }
    }

    //Make a fade whith an animationCurve and reset the over fade
    void Fade(ref float timeFade, AnimationCurve animationCurve, ref float inverseTimeFade)
    {
        timeFade += Time.deltaTime;
        gameObject.transform.localScale = new Vector3(animationCurve.Evaluate(timeFade) * maxScale.x, animationCurve.Evaluate(timeFade) * maxScale.y, animationCurve.Evaluate(timeFade) * maxScale.z);
        inverseTimeFade = 0;
    }
}
