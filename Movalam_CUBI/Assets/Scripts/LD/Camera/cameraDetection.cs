using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraDetection : MonoBehaviour
{
    [SerializeField] private float percentageDetection = 0.2f;

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.gameObject.GetComponentInChildren<DetectionController>().AddDetection(percentageDetection, false);
        }
    }
}
