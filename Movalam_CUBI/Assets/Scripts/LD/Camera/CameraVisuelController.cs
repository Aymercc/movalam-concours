using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraVisuelController : MonoBehaviour
{
    [SerializeField] private Transform areaToLook;          // Une r�f�rence sur le transform de la zone de d�tection

    void Update()
    {
        transform.LookAt(areaToLook);
    }
}
