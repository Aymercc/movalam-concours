using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class LevelRuleSet : NetworkBehaviour
{
    /// <summary>
    ///     This Game Rule Set is the reference to the levelAsset Game Rule Set and should not be modified at runtime
    /// </summary>
    public GameRuleSet gameRuleSet;
    /// <summary>
    ///     This Game Rule Set data is meant to be change at runtime 
    /// </summary>
    [SyncVar]
    public GameRuleSet levelRuleSet;

    private static LevelRuleSet instance;
    public static LevelRuleSet Instance {
        get {
            if (instance)
                return instance;
            else
            {
                Debug.LogError("No rule set instantiated");
                return null;
            }
        }
    }

    private void Awake()
    {
        if (!instance)
        {
            instance = this;
            if (LobbyManager.Instance != null)
            {
                instance.gameRuleSet = LobbyManager.Instance.currentLevel.levelRuleSet;
            }
            else
            {
                GameRuleSet gameRuleSet = Resources.Load<GameRuleSet>("GameRuleSets/InitialGameRuleSet");
                instance.gameRuleSet = gameRuleSet;
            }
            instance.Init();
        }
    }

    private void Init()
    {
        levelRuleSet = Instantiate(gameRuleSet);
    }
}
