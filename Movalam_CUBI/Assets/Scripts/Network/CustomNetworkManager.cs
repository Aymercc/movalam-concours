using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using Steamworks;
using Steamworks.Data;
using System.Linq;
using UnityEngine.SceneManagement;

public enum Role
{
    Assistant,
    Agent,
    Error
}

public struct NetworkIdentityLink
{
    public NetworkIdentity networkIdentity;
    public SteamId steamId;
}

public class CustomNetworkManager : NetworkManager
{
    public List<GameObject> spawns;
    public GameObject AssistantPrefab;
    public GameObject AgentPrefab;

    private List<NetworkAssistant> assistantControllers = new List<NetworkAssistant>();
    private List<NetworkIdentityLink> newAgentIdentityLink = new List<NetworkIdentityLink>();


    public delegate void ClientError();
    public static ClientError OnClientErrorEvent;

    #region Server system callbacks
    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        Friend member = GetMember(conn);

        Role playerRole = SelectPrefab(member);
        string slotIndexData = LobbyManager.Instance.currentLobby.GetMemberData(member, "slotIndex");
        int slotIndex = int.Parse(slotIndexData);
        GameObject player;
        if (playerRole == Role.Assistant)
        {
            player = Instantiate(AssistantPrefab, Vector3.zero, Quaternion.identity);
            // On garde une r�ference sur les scripts assistants pour ajouter les net ids des agents

            NetworkAssistant networkAssistant = player.GetComponent<NetworkAssistant>();
            networkAssistant.steamId = member.Id;

            assistantControllers.Add(networkAssistant);


            NetworkServer.Spawn(player);
            NetworkServer.AddPlayerForConnection(conn, player);

            // Si des agents ont �t� spawn avant l'assistant, on ajoute les net ids au nouvel l'assistant
            foreach (NetworkIdentityLink link in newAgentIdentityLink)
                assistantControllers[assistantControllers.Count - 1].networkIdentityLinks.Add(link);
        }
        else
        {
            slotIndex -= LobbyManager.Instance.currentLevel.AssitantCount;
            //Get Spawn Position with Slot index
            //string slotIndexData = LobbyManager.Instance.currentLobby.GetMemberData(member, "slotIndex");
            Vector3 agentSpawnPosition = Vector3.zero;
            if(!string.IsNullOrEmpty(slotIndexData))
            {
                //int slotIndex = int.Parse(slotIndexData) - LobbyManager.Instance.currentLevel.AssitantCount;
                if (slotIndex >= 0 && slotIndex < spawns.Count) {
                    agentSpawnPosition = spawns[slotIndex].transform.position;
                }
            }
            else
            {
                int i = 0;
                while (i < LobbyManager.Instance.slots.Length)
                {
                    if (LobbyManager.Instance.slots[i].isAvailable || LobbyManager.Instance.slots[i].takenBy.ToString() == conn.address)
                        break;
                    i++;
                }
                if(i < LobbyManager.Instance.slots.Length)
                    agentSpawnPosition = spawns[i].transform.position;
            }

            //Instantiate
            player = Instantiate(AgentPrefab, agentSpawnPosition, Quaternion.identity);

            //Init Network Agent
            NetworkAgent networkAgentScript = player.GetComponent<NetworkAgent>();
            networkAgentScript.playerName = member.Name;
            networkAgentScript.steamId = member.Id;
            networkAgentScript.movementController.SetCheckpoint(agentSpawnPosition);


            // On garde les netIds des agents pour les ajouter aux futurs assistants
            NetworkIdentityLink link = new NetworkIdentityLink { networkIdentity = player.GetComponent<NetworkIdentity>(), steamId = member.Id };

            // On ajoute les netIds de l'agent aux assistants
            foreach (NetworkAssistant assistantController in assistantControllers)
                assistantController.networkIdentityLinks.Add(link);


            NetworkServer.Spawn(player);
            NetworkServer.AddPlayerForConnection(conn, player);

            networkAgentScript.networkIdentityLinks.Add(link);
            foreach (NetworkIdentityLink networkLink in newAgentIdentityLink)
            {
                networkAgentScript.networkIdentityLinks.Add(networkLink);
                networkLink.networkIdentity.GetComponent<NetworkAgent>().networkIdentityLinks.Add(link);
            }

            newAgentIdentityLink.Add(link);
        }
    }

    public override void OnServerConnect(NetworkConnection conn)
    {
        Lobby lobby = LobbyManager.Instance.currentLobby;
        SteamId steamId = conn.address == "localhost" ? SteamClient.SteamId : (SteamId)ulong.Parse(conn.address);
        Friend member = lobby.Members.First(member => member.Id == steamId);
        Debug.LogFormat("Receive Connection from Player {0} (SteamId: {1})", member.Name, steamId);
        base.OnServerConnect(conn);
    }

    public override void OnServerReady(NetworkConnection conn)
    {
        base.OnServerReady(conn);
        if (conn.address != "localhost") return;
        LobbyManager.Instance.currentLobby.SetGameServer(LobbyManager.Instance.currentLobby.Owner.Id);
        LobbyManager.Instance.currentLobby.SetData("startGame", (true).ToString());
        spawns = GameObject.FindGameObjectsWithTag("AgentSpawn").ToList();
        spawns.Sort((a, b) => a.name.CompareTo(b.name));
        GameObject levelRulesetPrefab = spawnPrefabs.Find(prefab => prefab.name == "Level Rule Set");
        if(levelRulesetPrefab != null)
        {
            GameObject levelRulesetInstantiate = Instantiate(levelRulesetPrefab);
            NetworkServer.Spawn(levelRulesetInstantiate);
        }
        else
        {
            Debug.Log("Level Rule Set prefab not found");
        }
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        base.OnServerDisconnect(conn);
        foreach (NetworkAssistant assistantController in assistantControllers)
            assistantController.networkIdentityLinks.Remove(assistantController.networkIdentityLinks.Find(link => link.steamId.ToString() == conn.address));

        foreach (NetworkIdentityLink agentLink in newAgentIdentityLink)
        {
            NetworkAgent networkAgent = agentLink.networkIdentity.GetComponent<NetworkAgent>();
            networkAgent.networkIdentityLinks.Remove(networkAgent.networkIdentityLinks.Find(link => link.steamId.ToString() == conn.address));
        }
        newAgentIdentityLink.Remove(newAgentIdentityLink.Find(link => link.steamId.ToString() == conn.address));
    }
    public override void OnStopServer()
    {
        base.OnStopServer();
        newAgentIdentityLink.Clear();
        assistantControllers.Clear();
        LobbyManager.Instance.currentLobby.SetData("startGame", (false).ToString());
        LobbyManager.Instance.currentLobby.SetData("syncStart", (false).ToString());
    }

    public override void OnServerSceneChanged(string sceneName)
    {
        base.OnServerSceneChanged(sceneName);
        LoadingScreen.HideLoadingScreen(() => { });
        if (LobbyManager.Instance.currentLobby.Id != 0)
        {
            LobbyManager.Instance.SetConnected(SceneManager.GetActiveScene().name == onlineScene);
        }
    }

    #endregion

    #region Client system callbacks

    public override void OnClientConnect(NetworkConnection conn)
    {
        // OnClientConnect by default calls AddPlayer but it should not do
        // that when we have online/offline scenes. so we need the
        // clientLoadedScene flag to prevent it.
        if (!clientLoadedScene)
        {
            // Ready/AddPlayer is usually triggered by a scene load completing. if no scene was loaded, then Ready/AddPlayer it here instead.
            if (!NetworkClient.ready) NetworkClient.Ready();
            NetworkClient.AddPlayer();
        }
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
        LobbyManager.Instance.EndGame();
        LobbyManager.Instance.currentLobby.SetMemberData("isConnected", (false).ToString());
        if (!NetworkClient.isConnected)
        {
            OnClientErrorEvent?.Invoke();
        }
    }

    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        base.OnClientSceneChanged(conn);
        LoadingScreen.HideLoadingScreen(() => { });
        if(LobbyManager.Instance.currentLobby.Id != 0)
        {
            LobbyManager.Instance.SetConnected(SceneManager.GetActiveScene().name == onlineScene);
        }
    }
    #endregion


    public Friend GetMember(NetworkConnection conn)
    {
        SteamId steamId = conn.address == "localhost" ? SteamClient.SteamId : (SteamId)ulong.Parse(conn.address);
        return LobbyManager.Instance.currentLobby.Members.First(member => member.Id == steamId);
    }

    public Role SelectPrefab(Friend member)
    {
        string slotIndexData = LobbyManager.Instance.currentLobby.GetMemberData(member, "slotIndex");
        if (slotIndexData != "")
        {
            int slotIndex = int.Parse(slotIndexData);
            string lobbyLevel = LobbyManager.Instance.currentLobby.GetData("level");
            LevelAsset levelAsset = Resources.Load<LevelAsset>("LevelAssets/" + lobbyLevel);
            if (slotIndex < levelAsset.AssitantCount)
            {
                return Role.Assistant;
            }
            else
            {
                return Role.Agent;
            }
        }
        else
        {
            // slotIndex data missing
            Debug.LogError("Slot Index lobby member data missing");
            return Role.Error;
        }
    }


}
