using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class GameOverReceiver : NetworkBehaviour
{
    public AK.Wwise.State gameOverState;
    public AK.Wwise.RTPC effectVolume;

    private void Start()
    {
        NetworkClient.RegisterHandler<GameOverMessage>(ReceiveGameOver);
    }

    [ContextMenu("Local Game Over")]
    public void GameOverContextMenu()
    {
        ReceiveGameOver(new GameOverMessage { });
    }

    void ReceiveGameOver(GameOverMessage msg)
    {
        // Game over local
        GameObject gameOverObject = GameObject.FindGameObjectWithTag("GameOver");

        if (gameOverObject)
        {
            effectVolume.SetGlobalValue(0);
            gameOverState.SetValue();
            MovementController movementController = GetComponentInChildren<MovementController>();
            if (movementController) // On d�sactive le characterController si on est un agent
            {
                movementController.enabled = false;
                GetComponentInChildren<Animator>().enabled = false;
            }
            else // Sinon on d�sactive le d�placement de cam�ra de l'assistant
            {
                GetComponentInChildren<AssistantCameraController>().enabled = false;
            }

            gameOverObject.GetComponent<Canvas>().enabled = true;
            gameOverObject.GetComponent<ControllerGameOver>().Initialize();
        }
    }
}
