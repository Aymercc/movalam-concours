using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class NetworkPorte : NetworkGenericTerminal
{
    private DecodeurPorte decodeurPorte;

    private void Start()
    {
        base.Awake();
        decodeurPorte = GetComponentInChildren<DecodeurPorte>();
        decodeurPorte.OnOpenDoorEvent += OpenDoor;
        decodeurPorte.OnFailedPassword += OnPasswordFailed;
    }

    public override void Update()
    {
        if (!decodeurPorte.GetOpened())
            base.Update();
    }

    #region Open Door
    public void OpenDoor()
    {
        if (isServer)
        {
            ClientRpcOpenDoor();
        }
        else
        {
            CmdDesactivateDetector();
        }
    }
    
    [ClientRpc]
    public void ClientRpcOpenDoor()
    {
        decodeurPorte.OuvrirPorte();
        GetComponent<BoxCollider>().enabled = false;
    }
    #endregion

    #region Password Failed
    public void OnPasswordFailed()
    {
        if (isServer)
        {
            RaiseDetection();
        }
        else
        {
            CmdPasswordFailed();
        }
    }

    [Command]
    public void CmdPasswordFailed()
    {
        RaiseDetection();
    }

    [ClientRpc]
    public void RaiseDetection()
    {
        NetworkAgent local = NetworkAgent.localAgent;
        if (local != null)
        {
            local.detectionController.AddDetection(decodeurPorte.percentageDetection, false, true);
        }
    }
    #endregion

    [Command]
    public void CmdDesactivateDetector()
    {
        ClientRpcOpenDoor();
    }

}
