using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public struct EndMessage : NetworkMessage
{
}

public class Endgame : NetworkBehaviour
{
    public int agentCount = 0;
    public List<NetworkIdentity> agentIdsInEndBox = new List<NetworkIdentity>();
    public RectTransform textRecTransform;
    public Vector3 minScale;
    public Vector3 maxScale;
    private AssistantCameraController assistantCameraController;
    private float normalization;
    public AK.Wwise.RTPC rtcpSFX;

    private void Start()
    {
        agentCount = LobbyManager.Instance.currentLevel.AgentCount;
    }

    private void Update()
    {
        if (NetworkAssistant.assistant == null) return;
        else if (assistantCameraController == null)
        {
            assistantCameraController = NetworkAssistant.assistant.asssitantCameraController;
        }
        normalization = (assistantCameraController.transform.position.y - assistantCameraController.MinHeight) / (assistantCameraController.MaxHeight - assistantCameraController.MinHeight);
        textRecTransform.localScale = Vector3.Lerp(minScale, maxScale, normalization);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isServer && other.CompareTag("Player"))
        {
            NetworkIdentity netId = other.gameObject.GetComponentInParent<NetworkIdentity>();
            if (!agentIdsInEndBox.Contains(netId))
            {
                agentIdsInEndBox.Add(netId);

                if (agentIdsInEndBox.Count == agentCount)
                {
                    // C'est la fin
                    rtcpSFX.SetGlobalValue(0);
                    NetworkServer.SendToAll<EndMessage>(new EndMessage { });
                }
            }
        }
    }

    [ContextMenu("Send End")]
    private void SendEndMessage()
    {
        if (isServer)
            NetworkServer.SendToAll<EndMessage>(new EndMessage { });
    }

    private void OnTriggerExit(Collider other)
    {
        if (isServer && other.CompareTag("Player"))
        {
            NetworkIdentity netId = other.gameObject.GetComponentInParent<NetworkIdentity>();
            agentIdsInEndBox.Remove(netId);
        }
    }
}
