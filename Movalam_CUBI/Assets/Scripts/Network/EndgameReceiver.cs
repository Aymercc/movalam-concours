using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class EndgameReceiver : NetworkBehaviour
{
    public AK.Wwise.RTPC rtcpSFX;
    private void Start()
    {
        NetworkClient.RegisterHandler<EndMessage>(End);
    }

    [ContextMenu("Local End")]
    public void EndContextMenu()
    {
        End(new EndMessage { });
    }

    public void End(EndMessage msg)
    {
        // Fin local
        rtcpSFX.SetGlobalValue(0);
        Canvas endMenu = GameObject.FindGameObjectWithTag("EndDialog").GetComponent<Canvas>();
        if (endMenu)
        {
            MovementController movementController = GetComponentInChildren<MovementController>(); // On d�sactive le characterController et l'UX si on est un agent
            if (movementController)
            {
                movementController.enabled = false;
                GetComponentInChildren<Animator>().enabled = false;
                transform.Find("Agent/CanvasUXAgent").gameObject.SetActive(false);
            }
            else // Sinon on d�sactive le d�placement de cam�ra et l'UX de l'assistant
            {
                GetComponentInChildren<AssistantCameraController>().enabled = false;
                transform.Find("Assistant/GameplayCanvas").gameObject.SetActive(false);
            }

            endMenu.enabled = true;
            endMenu.GetComponentInChildren<DialogWriter>().Initialize();
        }
    }
}
