using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomLetter : MonoBehaviour
{
    private List<string> letterList;                                         // La liste des lettres disponibles

    [SerializeField] private ButtonCombinationController buttonScript;  // Une r�f�rence sur le script du bouton pour lui envoyer son num�ro

    public static List<string> letterListAlreadyTaken = new List<string>();

    /*void Start()
    {
        string letterChosen = letterList[Random.Range(0, letterList.Count)];
        GetComponent<Text>().text = letterChosen;
        buttonScript.SetLetter(letterChosen);
    }*/

    public void Initialize(List<string> alphabet)
    {
        letterList = alphabet;
        string letterChosen = letterList[Random.Range(0, letterList.Count)];
        while (letterListAlreadyTaken.Contains(letterChosen))
        {
            letterChosen = letterList[Random.Range(0, letterList.Count)];
        }
        letterListAlreadyTaken.Add(letterChosen);
        GetComponent<Text>().text = letterChosen;
        buttonScript.SetLetter(letterChosen);
        buttonScript.SetAlphabet(alphabet);
    }

    public void refreshTakenLetter()
    {
        letterListAlreadyTaken = new List<string>();
    }
}
