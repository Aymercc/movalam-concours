using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAlphabetList : MonoBehaviour
{
    private string[] alphabetList = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "v", "x", "y", "z" };
    private List<string> randomAlphabet;
    private string randomLetter;

    [SerializeField] private WheelController wheel;
    [SerializeField] private CombinaisonPanelController combiPanel;

    private void Start()
    {
        randomAlphabet = new List<string>(); 
        while(randomAlphabet.Count != 10)
        {
            randomLetter = alphabetList[Random.Range(0, alphabetList.Length-1)];
            if(!randomAlphabet.Contains(randomLetter))
            {
                randomAlphabet.Add(randomLetter);
            }
        }

        wheel.Initialise(randomAlphabet);
        combiPanel.Initialise(randomAlphabet);
        // Et l� on essaie de tout r�cup�rer
    }
}
