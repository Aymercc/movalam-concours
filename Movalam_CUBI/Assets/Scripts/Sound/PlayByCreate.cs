using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayByCreate : MonoBehaviour
{
    public AK.Wwise.Event wwiseEvent;
    public Button button;
    public void PlayTheEvent()
    {
        if (button.interactable)
        {
            wwiseEvent.Post(gameObject);
        }
    }
}
