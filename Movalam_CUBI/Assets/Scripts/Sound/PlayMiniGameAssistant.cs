using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMiniGameAssistant : MonoBehaviour
{
    public GameObject miniGame;
    public AK.Wwise.Event wwiseEvent;

    private bool done = false;

    void Update()
    {
        if (miniGame.activeSelf && !done)
        {
            done = true;
            wwiseEvent.Post(gameObject);
        }
        if (!miniGame.activeSelf)
        {
            done = false;
        }
    }
}
