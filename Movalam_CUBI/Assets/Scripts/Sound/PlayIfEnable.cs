using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayIfEnable : MonoBehaviour
{
    public Canvas canvasToBeEnable;
    public AK.Wwise.Event wwiseEvent;

    private bool done = false;

    void Update()
    {
        if (canvasToBeEnable.enabled && !done)
        {
            done = true;
            wwiseEvent.Post(gameObject);
        }
        if (!canvasToBeEnable.enabled)
        {
            done = false;
        }
    }
}
