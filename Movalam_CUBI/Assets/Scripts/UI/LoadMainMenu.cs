using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;

public class LoadMainMenu : MonoBehaviour
{
    [SerializeField] private SceneField mainMenuScene;              // Une r�f�rence sur la sc�ne de menu principal

    public void LoadMainMen()
    {
        Cursor.visible = true;
        if (CustomNetworkManager.singleton.isNetworkActive)
        {
            if (NetworkServer.active)
            {
                CustomNetworkManager.singleton.StopHost();
            }
            else
            {
                CustomNetworkManager.singleton.StopClient();
            }
        }
        else
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
