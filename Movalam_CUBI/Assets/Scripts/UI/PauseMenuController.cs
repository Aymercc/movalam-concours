using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;

public class PauseMenuController : MonoBehaviour
{
    [Header("Cursors")]
    [SerializeField] private Transform horizontalCursorBar;                 // R�f�rence sur la barre horizontale du curseur
    [SerializeField] private Transform verticalCursorBar;                   // R�f�rence sur la barre verticale du curseur

    private MovementController movementScript;                              // R�f�rence sur le script de mouvement du joueur pour l'emp�cher de bouger
    private AssistantCameraController cameraAssistant;                      // R�f�rence sur le script de la cam�ra assistant pour la d�sactiver

    public GameObject settingsPanel;                                        // R�f�rence sur le gameobjet setting panel
    public GameObject[] objectToActivate;                                   // Les objets � activer apr�s retour

    private bool check = false;

    public void Pause(MovementController movement, AssistantCameraController assistant)
    {
        if (movement != null)
        {
            movementScript = movement;
            movementScript.Pause();
        }
        if (assistant != null)
        {
            cameraAssistant = assistant;
            cameraAssistant.Pause();
        }

        check = true;
    }

    private void Update()
    {
        if (check)
        {
            Cursor.visible = false;
            verticalCursorBar.position = new Vector3(Input.mousePosition.x, verticalCursorBar.position.y, verticalCursorBar.position.z);
            horizontalCursorBar.position = new Vector3(horizontalCursorBar.position.x, Input.mousePosition.y, horizontalCursorBar.position.z);
        }
    }

    public void Resume()
    {
        if (movementScript != null)
        {
            movementScript.gameObject.GetComponent<PauseController>().IsNotOpen();
            movementScript.Resume();
        }
        if (cameraAssistant != null)
        {
            cameraAssistant.pauseCont.IsNotOpen();
            cameraAssistant.Resume();
        }

        movementScript = null;
        cameraAssistant = null;

        foreach (GameObject GO in objectToActivate)
        {
            GO.SetActive(true);
        }

        check = false;
        Cursor.visible = true;
        gameObject.GetComponent<Canvas>().enabled = false;
        settingsPanel.SetActive(false);
    }

    public void Exit()
    {
        // Renvoyer dans le lobby 
        Cursor.visible = true;
        if (CustomNetworkManager.singleton.isNetworkActive)
        {
            if (NetworkServer.active)
            {
                CustomNetworkManager.singleton.StopHost();
            }
            else
            {
                CustomNetworkManager.singleton.StopClient();
            }
        }
        else
        {
            SceneManager.LoadScene("MainMenu");
        }
    }

    // Faudra pas oublier de faire r�apparaitre le curseur � la fin
}
