using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{
    private GameObject[] slides;                    //Un tableau contenant tous les slides pour qu'on puisse naviguer
    private int currentIndex;

    [SerializeField] private GameObject leftButton;
    [SerializeField] private GameObject rightButton;

    void OnEnable()
    {
        currentIndex = 0;
        leftButton.SetActive(false);
        rightButton.SetActive(true);
        slides = GameObject.FindGameObjectsWithTag("Slide");
        //Debug.Log(slides.Length);

        HideAllSlideExceptFirst();
    }

    private void HideAllSlideExceptFirst()
    {
        for (int i = 1; i < slides.Length; i++)
        {
            slides[i].SetActive(false);
        }
    }

    public void ShowAllSlides()
    {
        for (int i = 0; i < slides.Length; i++)
        {
            slides[i].SetActive(true);
        }
    }

    public void NextSlide()
    {
        slides[currentIndex].SetActive(false);
        currentIndex++;
        slides[currentIndex].SetActive(true);

        leftButton.SetActive(true);
        if (currentIndex == slides.Length - 1)
            rightButton.SetActive(false);
    }

    public void PreviousSlide()
    {
        slides[currentIndex].SetActive(false);
        currentIndex--;
        slides[currentIndex].SetActive(true);

        rightButton.SetActive(true);
        if (currentIndex == 0)
            leftButton.SetActive(false);
    }
}
