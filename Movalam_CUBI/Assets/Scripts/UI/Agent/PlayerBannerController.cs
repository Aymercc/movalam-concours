using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;
using TMPro;
using VivoxUnity;
using Steamworks;

public class PlayerBannerController : MonoBehaviour
{
    public Image detectionBar;
    public float timeForFullToEmpty
    {
        get
        {
            if (LevelRuleSet.Instance)
                return LevelRuleSet.Instance.levelRuleSet.detectionTime;
            else
                return 5;
        }
    }

    public TextMeshProUGUI playerText;                                          // Une r�f�rence sur le composant Text de la player frame
    public Image playerAvatar;
    private NetworkIdentity playerNetId;                                        // NetworkId du joueur pour pouvoir lui envoyer des emotes

    [Header("Speech Feedback")]
    public Image speechFeedback;
    public float feedbackFadeDuration = 1;
    private float feedbackValue;
    public bool isTalking { get; private set; }
    private Color originalColor;

    private void Start()
    {
        VivoxManager.Instance.Vivox.OnSpeechDetectedEvent += OnSpeech;
        originalColor = speechFeedback.color;
        speechFeedback.color = Color.clear;
        feedbackFadeDuration = feedbackFadeDuration > 0 ? feedbackFadeDuration : 0.1f;
    }
    private void OnDestroy()
    {
        VivoxManager.Instance.Vivox.OnSpeechDetectedEvent -= OnSpeech;
    }
    private void Update()
    {
        if (isTalking)
        {
            feedbackValue = 1;
            speechFeedback.color = originalColor;
        }
        else
        {
            feedbackValue -= (1 / feedbackFadeDuration) * Time.deltaTime;
            speechFeedback.color = Color.Lerp(Color.clear, originalColor, feedbackValue);
        }
    }

    public void SetPlayerBanner(string name, Sprite avatar)
    {
        playerText.text = name;
        playerAvatar.sprite = avatar;
        playerAvatar.transform.localScale = new Vector3(1, -1, 1);
    }

    public NetworkIdentity GetNetworkIdentity()
    {
        return playerNetId;
    }

    public void SetNetworkdIdentity(NetworkIdentity netId)
    {
        playerNetId = netId;

        // On cherche le player qu'on ajoute dans la scene
        foreach(GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            NetworkAgent netAgent = player.GetComponent<NetworkAgent>();
            if (netAgent && netAgent.netIdentity == netId)
            {
                // Lorsque ce player a sa detection de changée, il l'envoie sur le réseau, le plyaer local le reçoit et Invoke le "SetDetection" qui modifie la barre
                netAgent.OnDetectionChangedEvent += SetDetection;
            }
        }
    }

    public void SetDetection(float detection)
    {
        detectionBar.fillAmount = detection / timeForFullToEmpty;
    }
    
    private void OnSpeech(string username, ChannelId channel, bool value)
    {
        // Si l'event concerne bien l'agent de cette carte et si il n'est pas l'agent local
        if (playerText.text != username || SteamClient.Name == username) return;
        isTalking = value;
    }
}
