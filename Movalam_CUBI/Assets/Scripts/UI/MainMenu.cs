using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;
public class MainMenu : MonoBehaviour
{
    public Transform levelAssetsCardsList;
    public GameObject levelAssetsCardPrefab;
    private LevelAsset selectedLevel;

    [Header("Lobby Creation Interface Reference")]
    public Transform levelSelector;
    public Transform lobbyParameters;
    public LobbyCreationUI LobbyCreationUI;

    [Header("Interface Reference")]
    public GameObject mainMenu;
    public GameObject lobbyInterface;
    public LobbyManagerUI lobbyManagerUI;

    public AK.Wwise.State mainMenuState;
    public AK.Wwise.Event playMusic;


    public AK.Wwise.RTPC musicVolume;
    public AK.Wwise.RTPC effectVolume;

    private void Awake()
    {
        if (PlayerPrefs.HasKey("musicVolume"))
            musicVolume.SetGlobalValue(PlayerPrefs.GetFloat("musicVolume"));
        else
            PlayerPrefs.SetFloat("musicVolume", 100);

        if (PlayerPrefs.HasKey("effectVolume"))
            effectVolume.SetGlobalValue(PlayerPrefs.GetFloat("effectVolume"));
        else
            PlayerPrefs.SetFloat("effectVolume", 100);

        foreach (LevelAsset levelAsset in LevelAssetsLoader.GetLevelAssets().Values)
        {
            LevelAssetUI levelAssetUI = Instantiate(levelAssetsCardPrefab, levelAssetsCardsList).GetComponent<LevelAssetUI>();
            levelAssetUI.LoadLevelAsset(levelAsset);
            levelAssetUI.cardSelectButton.onClick.AddListener(() => OnLevelSelected(levelAssetUI));
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        mainMenuState.SetValue();
        playMusic.Post(gameObject);
        try
        {
            if (!SteamClient.IsValid)
                return;
            if(SteamManager.Instance.localPlayer == null)
                SteamManager.Instance.localPlayer = SteamProfile.Create(SteamClient.SteamId, SteamClient.Name);
            if(LobbyManager.Instance.currentLobby.Id != 0)
            {
                mainMenu.SetActive(false);
                lobbyInterface.SetActive(true);
            }
        }
        catch (System.Exception e)
        {
            Debug.LogWarning("Unable to initializa Steam Client");
            Debug.LogWarning(e);
            // Something went wrong! Steam is closed?
        }
    }
    void OnLevelSelected(LevelAssetUI levelAssetUI)
    {
        //levelSelector.gameObject.SetActive(false);
        //lobbyParameters.gameObject.SetActive(true);
        //LobbyCreationUI.levelAssetUI.LoadLevelAsset(levelAsset);

        if(LobbyCreationUI.levelAssetUI)
            LobbyCreationUI.levelAssetUI.DeactiveBackground();
        LobbyCreationUI.levelAssetUI = levelAssetUI;

        LobbyCreationUI.levelAssetUI.ActiveBackground();
        LobbyCreationUI.OnLevelSelected();
    }
}
