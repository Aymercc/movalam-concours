using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditTextController : MonoBehaviour
{
    public void EndCredit()
    {
        GetComponentInParent<CreditController>().ReturnToMenu();
    }
}
