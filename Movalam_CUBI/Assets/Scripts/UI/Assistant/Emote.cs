using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Emote", menuName = "Emote")]
public class Emote : ScriptableObject
{
    public Sprite sprite;                               // L'image de l'emote
    public int numberUtilisation;                       // Le nombre d'utilisation avant que l'emote ne disparaisse

    [Header("Algorithm")]
    public bool useful;                                 // Emote utile si true, inutile si false
    [Range(1, 5)] public int weight;                    // La pond�ration de l'�mote (ie. la chance qu'elle apparaisse), 1 �tant le minimum, 
                                                        // 2 a deux fois plus de chance d'apparaitre que 1, 3 a 3 fois plus de chance que 1, etc...

    [Header("Minigame")]
    public int correspondingNumber;                     // Le nombre correspondant de l'emote
}
