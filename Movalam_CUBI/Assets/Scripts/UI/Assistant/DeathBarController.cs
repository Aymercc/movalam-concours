using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class DeathBarController : MonoBehaviour
{
    public Image deathBar;
    private float timeNeededToDie;                   // Le temps qu'il faut pour la barre pour se vider totalement
    private float startTime;
    private bool _timeOut = false;
    public MinigameAssistantController minigameController;           // Une r�f�rence sur le script du mini jeu pour mettre �Ejour la barre en temps r�elle

    public bool TimeOut {
        get { return _timeOut; }
    }


    void Start()
    {
        gameObject.SetActive(false);
        if (LevelRuleSet.Instance)
        {
            timeNeededToDie = LevelRuleSet.Instance.levelRuleSet.timeBeforeAgentDead;
        }
        else
        {
            timeNeededToDie = 20;
        }
    }

    void Update()
    {
        //Get current Time
        float currentTime;
        if (CustomNetworkManager.singleton)
            currentTime = (float)NetworkTime.time;
        else
            currentTime = Time.time;

        // Check Time out
        float elaspedTime = currentTime - startTime;
        if (elaspedTime >= timeNeededToDie)
        {
            if (!_timeOut)
                Death();
        }
        else
        {
            deathBar.fillAmount = 1 - elaspedTime / timeNeededToDie;

            if(minigameController != null)
            {
                if (minigameController.actualDedBeingSaved != null)
                {
                    if (minigameController.actualDedBeingSaved == GetComponentInParent<PlayerFrameController>())
                    {
                        minigameController.deathBar.fillAmount = 1 - elaspedTime / timeNeededToDie;
                        minigameController.timeValue.text = ((startTime + timeNeededToDie) - currentTime).ToString("F2");
                    }
                }
            }
        }
    }
    
    private void Death()
    {
        // Game over message
        GetComponentInParent<GameOver>().SendGameOver();

        _timeOut = true;
        gameObject.SetActive(false);
    }

    public void Detected(float time)
    {
        if(minigameController != null)
            timeNeededToDie = minigameController.startTimeBeforeDead;
        gameObject.SetActive(true);
        startTime = time;
        deathBar.fillAmount = 1;
        _timeOut = false;
    }

    public void Saved()
    {
        deathBar.fillAmount = 0;
        gameObject.SetActive(false);
    }


}
