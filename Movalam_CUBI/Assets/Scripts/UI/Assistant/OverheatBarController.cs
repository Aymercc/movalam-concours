using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OverheatBarController : MonoBehaviour
{
    [SerializeField] private float startOverheatCooldown;                 // La dur�e sans envoyer d'emote pour que la barre commence � decrease
    private float overheatCooldown;                                       // Le timer qui varie

    [SerializeField] private float timeForFullToEmpty;                    // Le temps souhait� pour que la barre se vide totalement
    [SerializeField] private int numberOfEmotesToFull;                    // Le nombre d'emote qu'on peut envoyer d'affil� avant de remplir totalement la barre de surchauffe

    private Slider overheatBar;                                           // Le slider de la barre de surchauffe


    void Start()
    {
        overheatCooldown = startOverheatCooldown;
        overheatBar = GetComponent<Slider>();
        overheatBar.maxValue = timeForFullToEmpty;
    }

    void Update()
    {
        if (overheatCooldown < startOverheatCooldown)   // Gestion du cooldown
        {
            overheatCooldown -= Time.deltaTime;
            if(overheatCooldown <= 0)
            {
                overheatCooldown = startOverheatCooldown;
            }
        }
        else if (overheatCooldown == startOverheatCooldown && overheatBar.value > 0)    // Si le cooldown emp�chant la barre de se vider est fini, alors elle se vide
        {
            if (overheatBar.value >= Time.deltaTime)
                overheatBar.value -= Time.deltaTime;
            else
                overheatBar.value = 0;
        }
    }

    public bool CanSendEmote()
    {
        if (timeForFullToEmpty - overheatBar.value > timeForFullToEmpty / numberOfEmotesToFull)
        {
            return true;
        }
        else
            return false;
    }

    public void SendEmote()
    {
        overheatBar.value += timeForFullToEmpty / numberOfEmotesToFull;
        overheatCooldown -= Time.deltaTime;
    }
}
