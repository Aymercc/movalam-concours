using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Steamworks;
using Mirror;
using VivoxUnity;

public class PlayerFrameController : MonoBehaviour
{
    [Header("Children")]
    public Text playerText;                                            // Une r�f�rence sur le composant Text de la player frame
    public Image playerImage;                                          // Une r�f�rence sur l'image du player frame
    public Image focusEye;

    [Header("Frame sprites")]
    public Sprite normalFrame;
    public Sprite focusFrame;

    [Header("Minigame")]
    public Button playerButton;
    public DeathBarController TimeToSaveBar;

    [Header("Voice feedback")]
    public Image speechFeedback;
    public float feedbackFadeDuration = 1;

    private LocalAssistant assistantController;                        // Une r�f�rence sur le script de l'assistant
    private float feedbackValue;
    private bool isTalking;
    private Color originalColor;
    private NetworkIdentity playerNetId;                               // NetworkId du joueur pour pouvoir lui envoyer des emotes
    private Transform parent;                                          // Une r�f�rence sur l'empty gameObject qu'est le parent
    private Color yellow = new Color(239/255, 200/255, 96/255, 1);
    private Color black = Color.black;
    private Image playerFrame;

    void Start()
    {
        yellow = playerText.color;
        playerFrame = GetComponent<Image>();
        assistantController = GetComponentInParent<LocalAssistant>();
        parent = gameObject.GetComponentInParent<Transform>();
        playerButton = GetComponent<Button>();
        VivoxManager.Instance.Vivox.OnSpeechDetectedEvent += OnSpeech;
        originalColor = speechFeedback.color;
        speechFeedback.color = Color.clear;
        feedbackFadeDuration = feedbackFadeDuration > 0 ? feedbackFadeDuration : 0.1f;
    }
    private void OnDestroy()
    {
        VivoxManager.Instance.Vivox.OnSpeechDetectedEvent -= OnSpeech;
    }

    private void Update()
    {
        if (isTalking)
        {
            feedbackValue = 1;
            speechFeedback.color = originalColor;
        }
        else
        {
            feedbackValue -= (1 / feedbackFadeDuration) * Time.deltaTime;
            speechFeedback.color = Color.Lerp(Color.clear, originalColor, feedbackValue);
        }
    }

    public void SetPlayerFrame(string name, Sprite avatar)
    {
        playerText.text = name;
        playerImage.sprite = avatar;
    }

    public NetworkIdentity GetNetworkIdentity()
    {
        return playerNetId;
    }

    public void SetNetworkdIdentity(NetworkIdentity netId)
    {
        playerNetId = netId;
    }

    public void ClickedFrame()
    {
        if (assistantController.GetSelectedPlayer() != null)
        {
            assistantController.GetSelectedPlayer().RemoveFocus();
        }
        // On met le joueur en focus, on aggrandit la case, et on stocke une r�f�rence pour envoyer des emotes au joueur
        SetFocus();
        assistantController.SetSelectedPlayer(this);
        TimeToSaveBar.minigameController = assistantController.minigameCanvas.gameObject.GetComponent<MinigameAssistantController>();
    }

    public void SetFocus()
    {
        //BigFrame();

        // Rendre visible l'oeil, changer le sprite, changer la couleur du texte
        focusEye.enabled = true;
        playerFrame.sprite = focusFrame;
        playerText.color = black;
    }

    public void RemoveFocus()
    {
        //SmolFrame();

        // Rendre invisible l'oeil, changer le sprite, changer la couleur du texte
        focusEye.enabled = false;
        playerFrame.sprite = normalFrame;
        playerText.color = yellow;
    }

    public void BigFrame()
    {
        parent.position = new Vector3(parent.position.x, parent.position.y + 15, parent.position.z);
        parent.localScale = new Vector3(parent.localScale.x + 0.1f, parent.localScale.y + 0.1f, parent.localScale.z);
    }

    public void SmolFrame()
    {
        parent.position = new Vector3(parent.position.x, parent.position.y - 15, parent.position.z);
        parent.localScale = new Vector3(parent.localScale.x - 0.1f, parent.localScale.y - 0.1f, parent.localScale.z);
    }

    private void OnSpeech(string username, ChannelId channel, bool value)
    {
        if (playerText.text != username) return;
        isTalking = value;
    }
}
