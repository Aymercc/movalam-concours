using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GradientMainMenu : MonoBehaviour
{
    private Gradient gradient;
    private GradientColorKey[] colorKey;
    private GradientAlphaKey[] alphaKey;

    void Start()
    {
        gradient = new Gradient();

        // Populate the color keys at the relative time 0 and 1 (0 and 100%)
        colorKey = new GradientColorKey[3];
        colorKey[0].color = Color.white;
        colorKey[0].time = 0.0f;
        colorKey[1].color = Color.white;
        colorKey[1].time = 0.5f;
        colorKey[2].color = Color.white;
        colorKey[2].time = 1.0f;

        // Populate the alpha  keys at relative time 0 and 1  (0 and 100%)
        alphaKey = new GradientAlphaKey[3];
        alphaKey[0].alpha = 100/255;
        alphaKey[0].time = 0.0f;
        alphaKey[1].alpha = 100/255;
        alphaKey[1].time = 0.5f;
        alphaKey[2].alpha = 0.0f;
        alphaKey[2].time = 1.0f;

        gradient.SetKeys(colorKey, alphaKey);
    }
}
